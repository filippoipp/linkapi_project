# linkapi_project

1 - Criar um arquivo .env.dev na pasta env.
2 - Copiar as envs de .env.example para .env.dev.
3 - yarn para installar as dependencias
4 - yarn dev para startar o serviço.

Foram criadas duas rotas:
1 -
`    method: 'post',
    path: '/v1/integration', `

onde a integraçao é feita. Os dados do pipedrive são enviados pro bling e posteriormente enviado para o mongo atlas.

2 -
`    method: 'get',
    path: '/v1/integration', `

que retorna os dados que estão salvos do mongo atlas.
