import { MongoClient } from 'mongodb'
import { ConnectionOptions } from 'tls';

export const startConnection = async (uri: string) => {
  await MongoClient.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectionOptions)
    .then(() => console.log('Database connection started.'))
    .catch((err: Error) => {
      console.log(err);
      throw err;
    });
};