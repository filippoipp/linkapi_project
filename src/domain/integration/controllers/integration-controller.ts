import { NextFunction, Request, Response } from "express"
import DatabaseService from "../services/db-service";
import IntegrationService from "../services/integration-service";

async function integratePlatforms(req: Request, res: Response, next: NextFunction) {
  try {
    const integrationService = new IntegrationService;
    const response = await integrationService.integratePlatforms();

    res.status(200).json(response);
  } catch (error) {
    next(error)
  }
}

async function getMongoData(req: Request, res: Response, next: NextFunction) {
  try {
    const databaseService = new DatabaseService;
    const response = await databaseService.getData();
    res.status(200).json(response);
  } catch (error) {
    next(error)
  }
}

export default { integratePlatforms, getMongoData }