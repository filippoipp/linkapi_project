import integrationController from "./controllers/integration-controller";

export default [
  {
    method: 'post',
    path: '/v1/integration',
    handlers: [
      integrationController.integratePlatforms,
    ],
  },
  {
    method: 'get',
    path: '/v1/integration',
    handlers: [
      integrationController.getMongoData,
    ],
  },
]