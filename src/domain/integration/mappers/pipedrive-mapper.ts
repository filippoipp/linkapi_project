export default class PipedriveMapper {
  static mapToBlingRequest(wonDeals: any): any {
    return {
      pedidos: wonDeals.map((deal: any) => ({
        pedido: {
          data: deal.won_time,
          cliente: {
            nome: deal.user_id.name,
          },
          itens: {
            item: {
              codigo: deal.title,
              descricao: deal.title,
              qtde: 1,
              vlr_unit: deal.value
            }
          }
        }
      }))
    }
  }
}