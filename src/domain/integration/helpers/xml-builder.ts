import xml2js from 'xml2js';

const xmlBuilder = (object: any): any => {
  const builder = new xml2js.Builder({ headless: false, renderOpts: { pretty: true }})
  const xml = builder.buildObject(object);

  return xml
}

export default xmlBuilder;