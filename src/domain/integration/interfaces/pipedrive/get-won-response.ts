export default interface IGetWonResponse {
  success: string;
  data: {}[],
  related_objects: {},
  additional_data: {}
}