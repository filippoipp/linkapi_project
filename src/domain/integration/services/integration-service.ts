import HttpError from "@errors/http-error";
import { integrationErrorKeys, integrationErrorMessages } from "@errors/translator/integration";
import { pipedriveErrorKeys, pipedriveErrorMessages } from "@errors/translator/pipedrive";
import xmlBuilder from "../helpers/xml-builder";
import PipedriveMapper from "../mappers/pipedrive-mapper";
import BlingService from "./bling-service";
import DatabaseService from "./db-service";
import PipedriveService from "./pipedrive-service";

export default class IntegrationService {
  async integratePlatforms(): Promise<any> {
    try {
      const pipedriveService = new PipedriveService;
      const wonDeals = await pipedriveService.getWon();
  
      if(wonDeals.data.length === 0) {
        throw new HttpError(
          400,
          pipedriveErrorKeys.WON_DEALS_NOT_FOUND,
          pipedriveErrorMessages[pipedriveErrorKeys.WON_DEALS_NOT_FOUND],
          {}
        )
      }
  
      const xml = xmlBuilder(PipedriveMapper.mapToBlingRequest(wonDeals.data));
  
      const blingService = new BlingService;
      await blingService.postRequest(xml);

      const requests = await blingService.getRequests();

      if(requests.retorno.pedidos) {
        const { pedidos } = requests.retorno;
        const databaseService = new DatabaseService;

        pedidos.forEach(pedido => {
          databaseService.insertData({ date: pedido.pedido.data, value: pedido.pedido.totalvenda})
        });
      }
  
      return { message: 'integraçao ok' }
    } catch (error) {
      console.log(error.report.body.retorno)
      throw new HttpError(
        400,
        integrationErrorKeys.DEFAULT_ERROR,
        integrationErrorMessages[integrationErrorKeys.DEFAULT_ERROR],
        {}
      )
    }
  }
}