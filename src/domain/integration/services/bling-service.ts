import Config from "@config/app-config";
import commonMethods from "@domain/common-methods";

export default class BlingService {
  async postRequest(xml: any): Promise<any> {

    const params = {
      apikey: process.env.BLING_API_KEY,
      xml
    }
    const url = `${Config.BLING.hostname}/${Config.BLING.paths.postRequest}`

    const response = await commonMethods.mountRequest('post', url, null, params);
    return response.data;
  }

  async getRequests(): Promise<any> {
    const params = {
      apikey: process.env.BLING_API_KEY,
    }
    const url = `${Config.BLING.hostname}/${Config.BLING.paths.getRequests}`

    const response = await commonMethods.mountRequest('get', url, null, params);
    return response.data;
  }
}