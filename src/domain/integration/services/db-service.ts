import HttpError from "@errors/http-error";
import { databaseErrorKeys, databaseErrorMessages } from "@errors/translator/database";
import mongoose from 'mongoose';

export default class DatabaseService {
  async insertData(requestData: any): Promise<any> {
    try {
      await mongoose.connect(process.env.MONGO_URI);
      var conn = mongoose.connection;
      await conn.collection('Request').insertOne(requestData)

      return
    } catch (error) {
      console.log(error.report.body.retorno)
      throw new HttpError(
        400,
        databaseErrorKeys.INSERT_FAIL,
        databaseErrorMessages[databaseErrorKeys.INSERT_FAIL],
        {}
      )
    }
  }

  async getData(): Promise<any> {
    await mongoose.connect(process.env.MONGO_URI);
    var conn = mongoose.connection;
    const data = await conn.collection('Request').find().toArray()

    return data;
  }
}