import Config from "@config/app-config";
import commonMethods from "@domain/common-methods";
import IGetWonResponse from "../interfaces/pipedrive/get-won-response";

export default class PipedriveService {
  async getWon(): Promise<IGetWonResponse> {
    const params = {
      api_token: process.env.PIPEDRIVE_TOKEN,
      status: 'won'
    }
    const url = `${Config.PIPEDRIVE.hostname}/${Config.PIPEDRIVE.paths.getWon}`

    const response = await commonMethods.mountRequest('get', url, null, params);
    return response.data as IGetWonResponse;
  }
}