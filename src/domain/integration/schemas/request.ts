import { Schema } from 'mongoose';

interface IRequest {
  date: string;
  value: string;
}

export const Request: Schema = new Schema<IRequest>({
  date: { type: String, required: true },
  value: { type: String, required: true },
});