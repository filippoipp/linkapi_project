import HttpError from '@errors/http-error';
import axios from 'axios';

type RequestTypes = 'get' | 'post' | 'put' | 'patch';
type BodyTypes = {};

const reportRequestError = (err: any) => ({
  response: {
    method: err.response.config.method,
    url: err.response.config.url,
    body: err.response.data,
  },
});

const captureErrors = (err: any) => {
  let errorCode: string;
  let errorMessage: string;

  throw new HttpError(err.response.status, errorCode, errorMessage, reportRequestError(err));
};

const mountRequest = async (
  type: RequestTypes,
  url: string,
  data?: BodyTypes,
  params?: object,
  headers?: object,
) => {
  try {
    if (type === 'get') {
      return await axios[type](url, { params, headers: { ...headers } });
    }
    return await axios[type](url, data, { params, headers: { ...headers } });
  } catch (error) {
    if (error.response) {
      captureErrors(error);
    }
    throw error;
  }
};

export default { mountRequest };