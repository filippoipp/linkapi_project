import env from 'dotenv';
import path from 'path';

env.config({
  path: path.join(__dirname, `../../env/.env.${process.env.NODE_ENV}`),
});
process.env.ENVIRONMENT = process.env.ENVIRONMENT || process.env.NODE_ENV;

class Config {
  public static DEV: boolean = process.env.ENVIRONMENT === 'dev';

  public static SERVERS = {
    http: {
      hostname: process.env.HTTP_HOST || 'localhost',
      port: parseInt(process.env.HTTP_PORT, 10) || 3000,
    },
  };

  public static PIPEDRIVE = {
    hostname: process.env.PIPEDRIVE_API_HOST,
    paths: {
      getWon: 'v1/deals'
    }
  }

  public static BLING = {
    hostname: process.env.BLING_API_HOST,
    paths: {
      postRequest: 'v2/pedido/json',
      getRequests: 'v2/pedidos/json'
    }
  }
}

export default Config;
